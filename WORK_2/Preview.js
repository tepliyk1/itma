Vue.component('preview', {
    props: {
        item: {
            type: Object,
            required: true
        },
        episodes: {
            type: Number
        }
    },
    data() {
        return {
            defImg: '' // картинка по умолчанию
        }
    },
    methods: {
        getImg() {
            return this.item.image_url ? this.item.image_url : this.defImg
        }
    },
    template: `
        <div :class="'col-2'">
            <div class="card">
                <img :src="getImg()" :alt="item.title" class="card-img-top">
                <div class="card-body">
                    <div class="card-title">{{ item.title }}</div>
                    <div class="card-text"> Епизод: {{ item.episodes}}</div>
                </div>
                <div class="card-footer">
                    <a :href="item.url" class="btn btn-primary active" >Подробнее</a>
                </div>
            </div>
        </div>
    `
});