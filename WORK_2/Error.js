Vue.component('error', {
    props: ['text'],
    template: `<div style="
    margin: 70px;
" class="alert alert-danger">{{ text }}</div>`
});