const vm = new Vue({
    el: '#app',
    components: {},

    data: {
        error: '',
        list: [],
        done: false,
        filterName: '',
    },


    created: function () {

        let self = this;

        setTimeout(
            loadData = function () {

                var Url = "https://api.jikan.moe/v3/top/anime";

                var request = $.ajax({
                    url: Url,
                    type: "GET",
                    dataType: "json"
                });

                request.done(function (msg) {
                    self.list = msg.top;
                    self.done = true;
                });

                request.fail(function (jqXHR, textStatus) {
                    self.error = "Request failed: " + textStatus;
                });
            }, 3000
        );


    },

    computed: {

        getAllData() {
            return this.list
        },
        Top10() {
            return this.list.slice(0, 10);
        },
        filterMovie() {
            return this.list.filter(item => item.type == 'Movie')
        },
        filterTV() {
            return this.list.filter(item => item.type == 'TV')
        },
        filterReverse() {
            return this.list.reverse();
        },


    },

    methods: {
        setFilter(name) {
            this.filter = name
        },
        clearData() {
            this.list = [];
        },
        loadData() {
            loadData();
        },
    },

    template: `
    <div class="container">
        <div class="row">
            <div class="col">
                <div class='btn-group-toggle' data-toggle="buttons">
                    <button class='btn' type="button" @click="clearData()">Очистить данные</button>
                    <button class='btn' type="button" @click="loadData()">Загрузить данные</button>
                </div>

                <div class='btn-group-toggle' data-toggle="buttons">
                    <button class='btn' type="button" @click="filterName='Top10'">Top10</button>
                    <button class='btn' type="button" @click="filterName='Movie'">Movie</button>
                    <button class='btn' type="button" @click="filterName='TV'">TV</button>
                    <button class='btn' type="button" @click="filterName='Reverse'">Reverse</button>
                    <button class='btn' type="button" @click="filterName=''">Все</button>
                </div>

            </div>
        </div>



        <div class="row" v-if="!done">
            <div class="col">
                <preloader></preloader>
            </div>
        </div>
        <div v-else>


            <div class="row" v-if="list.length == 0">
                <div class="col">
                    <nodata></nodata>
                </div>
            </div>

            <div class="row" v-else>

                <preview v-if="filterName == 'Top10'" v-for='(item, key) in Top10' :item="item"
                         :key="item.rank+'_filterTop10'"></preview>

                <preview v-else-if="filterName == 'Movie'" v-for='(item, key) in filterMovie' :item="item"
                         :key="item.rank+'_filterMovie'"></preview>

                <preview v-else-if="filterName == 'TV'" v-for='(item, key) in filterTV' :item="item"
                         :key="item.rank+'_filterTV'"></preview>
                         
                 <preview v-else-if="filterName == 'Reverse'" v-for='(item, key) in filterReverse' :item="item"
                         :key="item.rank+'_filterReverse'"></preview>

                <preview v-else v-for='(item, key) in getAllData' :item="item" :key="item.rank"></preview>

            </div>
        </div>
        <error v-show="error != ''" v-bind:text="error"></error>
    </div>
    `


});



